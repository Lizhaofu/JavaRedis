package com.lzf.mapper;


import com.lzf.model.User;

import java.util.List;


public interface UserMapper {
    public List<User> findUserInfo();
}
