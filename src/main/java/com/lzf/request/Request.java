package com.lzf.request;

/**
 * 请求接口
 * @author Administrator
 *
 */
public interface Request {
	
	void process();
	Integer getProductId();
	
}
