package com.lzf.service;


import com.lzf.model.User;

import java.util.List;

public interface IUserService {

    List<User> getUserInfo();

    User getCachedUserInfo();
}
