package com.lzf.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.lzf.mapper.UserMapper;
import com.lzf.model.User;
import com.lzf.service.IUserService;
import com.lzf.dao.RedisDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements IUserService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private RedisDAO redisDAO;

    @Override
    public List<User> getUserInfo() {
        return userMapper.findUserInfo();
    }

    @Override
    public User getCachedUserInfo() {
       // redisDAO.set("cached_user", "{\"name\": \"zhangsan\", \"age\": 25}") ;
        String json = redisDAO.get("cached_user");
        JSONObject jsonObject = JSONObject.parseObject(json);

        User user = new User();
        user.setName(jsonObject.getString("name"));
        user.setAge(jsonObject.getInteger("age"));

        return user;
    }
}
